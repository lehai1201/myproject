<?php
return [
    'guard' => [
        'frontend' => 'frontend',
        'backend' => 'backend'
    ],
    'delete_off' => 0,
    'delete_on' => 1
];