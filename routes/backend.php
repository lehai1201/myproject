<?php
use Illuminate\Support\Facades\Route;

Route::get('/login','LoginController@login')->name('management.login');
Route::post('/login','LoginController@post_login')->name('management.post.login');
Route::post('/register','LoginController@register')->name('management.register');
Route::get('/logout','LoginController@logout')->name('management.logout');
Route::get('/password_reissue','LoginController@passwordReissue')->name('management.password.reissue');
Route::post('/check_token','LoginController@checkToken')->name('management.check.token');
Route::get('/password_reissue/{token}','LoginController@changePasswordReissue')->name('management.change.new.password');
Route::post('/change_password','LoginController@postChangePassword')->name('management.post.new.password');
Route::group(array('middleware'=>'auth.backend'),function(){
    Route::get('/','HomeController@index')->name('management.home');
});