<?php

return [
    'required' => ' :attribute không được để trống',
    'required_with' => ' :attribute bắt buộc phải nhập ',
    'same' => ' :attribute và :other không trùng',
    'email' => ' :attribute phải nhập định dạng email',
    'numeric' => ' :attribute chỉ được nhập kiểu số nguyên',
    'custom' => [
        'attribute-name' => [

        ],
    ],

    'attributes' => [],
];
