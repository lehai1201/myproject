@extends('backend.layout.layout')
@section('content')
    <h1>Trang chủ</h1>
    <div class="row">
        <div class="col-md-12">

            <select id="example" multiple="multiple" style="width: 300px">
                <option value="JAN">January</option>
                <option value="FEB">February</option>
                <option value="MAR">March</option>
                <option value="APR">April</option>
                <option value="MAY">May</option>
                <option value="JUN">June</option>
                <option value="JUL">July</option>
                <option value="AUG">August</option>
                <option value="SEP">September</option>
                <option value="OCT">October</option>
                <option value="NOV">November</option>
                <option value="DEC">December</option>
            </select>

        </div>
    </div>

@endsection
@section('js')
    <script type="text/javascript">
        $('#example').select2({
            placeholder: 'Select a month',
        });
    </script>
@endsection