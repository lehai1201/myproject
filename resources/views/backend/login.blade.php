<!DOCTYPE html>
<html lang="vi">
<head>
    <title>Login Page</title>
    <!--Made with love by Mutiullah Samim -->
    <!--Bootsrap 4 CDN-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!--Fontawesome CDN-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <!--Custom styles-->
    <link rel="stylesheet" type="text/css" href="/css/login.css">
</head>
<body>
<div class="container">
    <div class="d-flex justify-content-center h-100">
        <div class="card">
            <div class="card-header">
                <h3 id="title_page_login">Sign In</h3>
                <h3 id="title_page_register" style="display: none">Register</h3>
                <div class="d-flex justify-content-end social_icon">
                    <span><i class="fab fa-facebook-square"></i></span>
                    <span><i class="fab fa-google-plus-square"></i></span>
                    <span><i class="fab fa-twitter-square"></i></span>
                </div>
            </div>
            <div class="card-body">
                <form id="form_login" autocomplete="off" method="POST" action="{{route('management.post.login')}}">
                    @csrf
                    @if(Session::has('message'))
                        <div style="padding: 10px" class="alert-success">{{Session::get('message')}}</div>
                        <br>
                    @endif
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="username" class="form-control" placeholder="username"
                               value="{{ old('username') }}">
                    </div>
                    @include('backend.element.validate_error',['message'=>$errors->first('username')])
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" name="my_password" class="form-control" placeholder="password"
                               value="{{ old('my_password') }}">
                    </div>
                    @include('backend.element.validate_error',['message'=>$errors->first('my_password')])
                    <div class="row align-items-center remember">
                        <input style="cursor: pointer" id="remember" name="remember" type="checkbox"><label
                                style="cursor: pointer" for="remember">Remember Me</label>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Login" class="btn float-right login_btn">
                    </div>
                    <div class="card-footer">
                        <div class=" links">
                            Don't have an account?<a href="#" onclick="choseRegister()">Sign Up</a>
                        </div>
                        <div class="links">
                            <a href="#" onclick="chosePassword()">Forget Password</a>
                        </div>
                    </div>
                </form>
                <form autocomplete="off" style="display: none" id="form_register" method="POST"
                      action="{{route('management.register')}}"
                >
                    @csrf
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" name="username_register" placeholder="username"
                               value="{{ old('username_register') }}">
                    </div>
                    @include('backend.element.validate_error',['message'=>$errors->first('username_register')])
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" name="password_register" placeholder="password"
                               value="{{ old('password_register') }}">
                    </div>
                    @if(null == ($errors->first('username_register')))
                        @include('backend.element.validate_error',['message'=>$errors->first('password_register')])
                    @endif
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" name="password_confirm"
                               placeholder="password confirm" value="{{ old('password_confirm') }}">
                    </div>
                    @include('backend.element.validate_error',['message'=>$errors->first('password_confirm')])
                    <div class="form-group">
                        <input type="submit" value="Register" class="btn float-right login_btn">
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-center links"><a href="#" onclick="choseRegister()">Sign
                                In</a>
                        </div>
                    </div>
                </form>
                <form autocomplete="off" action="{{route('management.password.reissue')}}" id="form_password_reissue"
                      style="display: none">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" name="email_reissue" placeholder="username">
                    </div>
                    @include('backend.element.validate_error',['message'=>$errors->first('email_reissue')])
                    <div class="form-group">
                        <input type="submit" value="Send" class="btn float-right login_btn">
                    </div>
                    <div class="d-flex justify-content-center links"><a href="#" onclick="chosePassword()">Sign
                            In</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        @if(null!=($errors->first('username_register')) || null!=($errors->first('password_register')) || null!=($errors->first('password_confirm')))
        $('form').css('display', 'none');
        $("#form_register").css('display', 'block');
        @endif
        @if(null!=($errors->first('email_reissue')))
        $('form').css('display', 'none');
        $("#form_password_reissue").css('display', 'block');
        @endif
    });

    function choseRegister() {
        $("#title_page_login").toggle();
        $("#title_page_register").toggle();
        $("#form_login").toggle();
        $("#form_register").toggle();
    }

    function chosePassword() {
        $("#form_login").toggle();
        $("#form_password_reissue").toggle();
    }
</script>
</body>
</html>