<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'SysAdmin',
                'code' => 'SA'
            ],
            [
                'name' => 'Admin',
                'code' => 'AD'
            ],
            [
                'name' => 'Employee',
                'code' => 'EP'
            ]
        ]);
    }
}
