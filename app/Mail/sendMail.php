<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class sendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try {
            return $this->from('ptit.lehai@gmail.com','Hai')->view('backend.mail.test_mail')->subject($this->data['title'])->with('data',$this->data);
        } catch (\Exception $e) {
            Log::warning('warnning', ['WARNNING' => $e->getMessage()]);
        }
    }
}
