<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = "permissions";
    protected $fillable = [
        'name', 'code', 'del_flag'
    ];

    public function roles()
    {
        return $this->belongsToMany(Roles::class,RolePermission::class, 'permission_id', 'role_id');
    }
}
