<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';
    protected $fillable = [
        'name', 'code', 'del_flag'
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, RolePermission::class, 'role_id', 'permission_id');
    }
    public function customers(){
        return $this->belongsToMany(Customer::class,UserRole::class,'role_id','customer_id');
    }
}
