<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    protected $table = 'customers';
    protected $fillable = [
        'username', 'email', 'password', 'address', 'phone', 'del_flag', 'email_token', 'email_valid_time'
    ];

    public function roles()
    {
        return $this->belongsToMany(Roles::class, UserRole::class, 'customer_id', 'role_id');
    }

    public function getRoles(){
        $role=array();
        if($this->roles()){
            $role=$this->roles()->get();
        }
        return $role;
    }
    public function hasPermission($permission_code)
    {
        $roles=$this->getRoles();
        $permission = Permission::where('code', $permission_code)->with('roles')->where('del_flag', config('const.delete_off'))->first();
        if(!empty($permission->roles)){
            foreach ($permission->roles as $per){
                if($roles->contains($per)){
                    return true;
                }
                return false;
            }
        };
        return false;
    }

}
