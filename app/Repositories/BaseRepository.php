<?php
namespace App\Repositories;

class BaseRepository
{
    protected $model_class;
    protected $check_del = true;

    public function __construct($model_class)
    {
        $this->model_class = $model_class;
    }

    //thực hiện câu truy vấn
    public function generateQueryBuilder($parameter, $relationship, $order_by)
    {
        $query_builder = $this->model_class::where('del_flag', config('const.delete_of'));
        if (!empty($parameter)) {
            foreach ($parameter as $key => $value) {
                $query_builder = $query_builder->where($key, $value);
            }
        }
        if (!empty($relationship)) {
            $query_builder = $query_builder->with($relationship);
        }
        if (!empty($order_by)) {
            if (in_array($order_by)) {
                foreach ($order_by as $order_key => $order_value) {
                    $query_builder = $query_builder->orderBy($order_key, $order_value);
                }
            }
        }
        return $query_builder;
    }

    //lấy tất cả data
    public function getDataItem($parameter, $relationship, $pagination, $orderBy)
    {
        $data_query = $this->generateQueryBuilder($parameter, $relationship, $orderBy);
        if (!empty($pagination)) {
            return $data_query->paginate($pagination);
        }
        return $data_query->get();
    }

    //lấy 1 bản ghi
}