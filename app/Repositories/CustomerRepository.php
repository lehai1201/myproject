<?php

namespace App\Repositories;

use App\Model\Customer;
use App\Repositories\BaseRepository;

class CustomerRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(Customer::class);
    }
}