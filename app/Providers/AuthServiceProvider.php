<?php

namespace App\Providers;

use App\Model\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $permission=$this->getPermission();
        if(!empty($permission)){
            foreach($permission as $per){
                Gate::define($per->code,function($user) use($per) {
                    return $user->hasPermission($per->code);
                });
            }
        }
    }

    public function getPermission()
    {
        $permissions = Permission::where('del_flag', config('const.delete_off'))->get();
        return $permissions;
    }
}
