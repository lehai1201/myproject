<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\BackendLoginRequest;
use App\Http\Requests\BackendRegisterRequest;
use App\Http\Requests\ChangePasswordReissue;
use App\Http\Requests\CheckEmailExits;
use App\Http\Requests\checkToken;
use App\Jobs\jobQueue;
use App\Mail\sendMail;
use App\Mail\SendPasswordReissue;
use App\Model\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    public function login()
    {
        if (Auth::guard('backend')->check()) {
            return redirect(route('management.home'));
        }
        return view('backend.login');
    }

    public function logout()
    {
        Auth::guard('backend')->logout();
        return redirect(route('management.login'));
    }

    public function post_login(BackendLoginRequest $request)
    {
        $username = $request->get('username');
        $password = $request->get('my_password');
        $remember = $request->get('remember');
        if (Auth::guard('backend')->attempt(['email' => $username, 'password' => $password, 'del_flag' => config('const.delete_off')], $remember)) {
            return redirect()->route('management.home');
        } else {
            return redirect()->back()->with('message', 'Sai thông tin đăng nhập');
        }
    }

    public function register(BackendRegisterRequest $request)
    {
        $email = $request->get('username_register');
        $password = $request->get('password_register');
        $customer = new Customer([
            'email' => $email,
            'password' => Hash::make($password)
        ]);
        $customer->save();
        return redirect(route('management.login'))->with('message', 'Đăng ký thành công, vui lòng đăng nhập');
    }

    public function passwordReissue(CheckEmailExits $request)
    {
        $email = $request->get('email_reissue');
        $user = Customer::where('email', $email)->where('del_flag', config('const.delete_off'))->first();
        if (!empty($user)) {
            if (empty($user->email_token)) {
                $ran = rand(1111, 9999);
                $email_token = Hash::make($ran);
                Customer::where('id', $user->id)->update([
                    'email_token' => $email_token,
                    'email_valid_time' => Carbon::now()
                ]);
                Mail::to($user->email)->send(new SendPasswordReissue($ran));
                return view('backend.password_reissue', ['id' => $user->id]);
            }
        }
    }

    public function checkToken(checkToken $request)
    {
        $id = $request->get('id');
        $user = Customer::find($id);
        if (!empty($user)) {
            $data = [
                'email_token' => $user->email_token,
                'id' => $user->id
            ];
            $token = http_build_query($data);
            $encrypt = encrypt($token);
            return redirect(route('management.change.new.password', $encrypt));
        }
    }

    public function changePasswordReissue($token)
    {
        $data = decrypt($token);
        $result = array();
        parse_str($data, $result);
        $user = Customer::find($result['id']);
        if (!empty($user)) {
            if ($user->email_valid_time <= Carbon::now()->subMinute(5)) {
                $id = $result['id'];
                return view('backend.change_password_reissue', compact('id'));
            } else {
                $user->update([
                    'email_token' => ''
                ]);
                return redirect(route('management.login'))->with('message', 'Token đã quá hạn, thử lại sau');
            }
        }
    }

    public function postChangePassword(ChangePasswordReissue $request)
    {
        $id = $request->get('id');
        $new_password = $request->get('new_password');
        $user = Customer::find($id);
        if (!empty($user)) {
            $user->update([
                'email_token' => '',
                'password' => Hash::make($new_password)
            ]);
            return redirect(route('management.login'))->with('message', 'Bạn đã thay đổi mật khẩu thành công, vui lòng đăng nhập lại');
        }
    }
}
