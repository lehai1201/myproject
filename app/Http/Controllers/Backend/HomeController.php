<?php

namespace App\Http\Controllers\Backend;

use App\Model\Customer;
use App\Model\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    public function index(){
//      if(!Gate::forUser(Auth::guard('backend')->user())->allows('CR')){
//            return redirect(route('management.test'));
//      }
        return view('backend.home.home');
    }
}
