<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    protected $customer_repository;

    public function __construct()
    {
        $this->customer_repository = new CustomerRepository();
    }

    public function index()
    {
        return view('frontend.home.home');
    }
}
