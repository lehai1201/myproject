<?php

namespace App\Http\Requests;

use App\Model\Customer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;

class BackendRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct(Factory $factory)
    {
        $factory->extend('check_email_exits',function(){
            $email=request()->get('username_register');
            $check=Customer::where('email',$email)->where('del_flag',config('const.delete_on'))->first();
            if(!empty($check)){
                return false;
            }
            return true;
        },'Tên tài khoản đã tồn tại');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username_register' => 'required|email|check_email_exits',
            'password_register' => 'required',
            'password_confirm'  =>'required_with:password_register| same:password_register'
        ];
    }
    public function attributes()
    {
        return [
            'username_register' => 'Tên tài khoản mới',
            'password_register' => 'Mật khẩu mới',
            'password_confirm' => 'Xác thực mật khẩu mới',
        ];
    }
}
