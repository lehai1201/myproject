<?php

namespace App\Http\Requests;

use App\Model\Customer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Factory;

class checkToken extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function __construct(Factory $factory)
    {
        $factory->extend('check_token',function(){
            $id=request()->get('id');
            $token=request()->get('token');
            $user=Customer::find($id);
            if(!empty($user)){
                if(Hash::check($token,$user->email_token)){
                    return true;
                }
            }
            return false;
        },'Token không đúng');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required|numeric|check_token'
        ];
    }
    public function attributes()
    {
        return [
          'token' => 'token'
        ];
    }
}
