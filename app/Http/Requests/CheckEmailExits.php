<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Factory as Factory;

class CheckEmailExits extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct(Factory $factory)
    {
        $factory->extend('check_email',function(){
            $email=request()->get('email_reissue');
            $check=DB::table('customers')->where('email',$email)->where('del_flag',config('const.delete_off'))->get();
            if($check->isEmpty()){
                return false;
            }
            return true;
        },'Mail không tồn tại');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email_reissue' => 'required|email|check_email'
        ];
    }

    public function attributes()
    {
        return [
            'email_reissue' => 'Tên tài khoản'
        ];
    }
}
