<?php

namespace App\Http\Requests;

use App\Model\Customer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Factory;

class ChangePasswordReissue extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function __construct(Factory $factory)
    {
        $factory->extend('check_old_password', function () {
            $id = request()->get('id');
            $new_password = request()->get('new_password');
            $user=Customer::find($id);
            if(!empty($user)){
                if(Hash::check($new_password,$user->password)){
                    return false;
                }
            }else{
                return false;
            }
            return true;
        },'Mật khẩu mới phải khác mật khẩu hiện tại');
    }

    public function rules()
    {
        return [
            'new_password' => 'required|min:4',
            'confirm_password' => 'required_with:new_password|same:new_password|check_old_password'
        ];
    }

    public function attributes()
    {
        return [
            'new_password' => 'Mật khẩu mới',
            'confirm_password' => 'Xác nhận mật khẩu mới'
        ];
    }
}
